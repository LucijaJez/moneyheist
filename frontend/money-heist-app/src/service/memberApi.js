const uri = "http://localhost:8080/member";
const jsonHeader = { "Content-Type": "application/json" };

export function fetchMembers() {
  return fetch(uri, {
    method: "GET",
  }).then((res) => res.json());
}

export function fetchMember(id) {
  return fetch(`${uri}/${id}`, {
    method: "GET",
  }).then((res) => res.json());
}

export function fetchMemberSkills(id) {
  return fetch(uri + `${uri}/${id}/skills`, {
    method: "GET",
  }).then((res) => res.json());
}

export function addMember(member) {
  return fetch(uri, {
    method: "POST",
    body: JSON.stringify(member),
    headers: jsonHeader,
  }).then((res) => res.headers.get("Location"));
}

export function updateMemberSkills(id, skills) {
  return fetch(`${uri}/${id}/skills`, {
    method: "PUT",
    body: JSON.stringify(skills),
    headers: jsonHeader,
  });
}

export function deleteMemberSkill(id, skillName) {
  return fetch(`${uri}/${id}/skills/${skillName}`, {
    method: "DELETE",
  });
}
