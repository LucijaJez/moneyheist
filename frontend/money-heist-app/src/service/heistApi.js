const uri = "http://localhost:8080/heist";
const jsonHeader = { "Content-Type": "application/json" };

export function fetchHeists() {
  return fetch(uri, {
    method: "GET",
  }).then((res) => res.json());
}

export function fetchHeist(id) {
  return fetch(`${uri}/${id}`, {
    method: "GET",
  }).then((res) => res.json());
}

export function fetchHeistMembers(id) {
  return fetch(`${uri}/${id}/members`, {
    method: "GET",
  }).then((res) => res.json());
}

export function fetchHeistSkills(id) {
  return fetch(`${uri}/${id}/skills`, {
    method: "GET",
  }).then((res) => res.json());
}

export function fetchHeistStatus(id) {
  return fetch(`${uri}/${id}/status`, {
    method: "GET",
  }).then((res) => res.json());
}

export function fetchHeistEligibleMembers(id) {
  return fetch(`${uri}/${id}/eligible_members`, {
    method: "GET",
  }).then((res) => res.json());
}

export function fetchHeistOutcome(id) {
  return fetch(`${uri}/${id}/outcome`, {
    method: "GET",
  }).then((res) => res.json());
}

export function addHeist(heist) {
  return fetch(uri, {
    method: "POST",
    body: JSON.stringify(heist),
    headers: jsonHeader,
  }).then((res) => res.headers.get("Location"));
}

export function updateHeistSkills(id, skills) {
  return fetch(`${uri}/${id}/skills`, {
    method: "PATCH",
    body: JSON.stringify(skills),
    headers: jsonHeader,
  });
}

export function confirmHeistMembers(id, memberNames) {
  return fetch(`${uri}/${id}/members`, {
    method: "PUT",
    body: JSON.stringify(memberNames),
    headers: jsonHeader,
  });
}

export function heistStart(id) {
  return fetch(`${uri}/${id}/start`, {
    method: "PUT",
  });
}
