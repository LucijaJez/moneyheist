import React from "react";
import { Link, useLocation } from "react-router-dom";
import cx from "classnames";

export default function Navigation() {
  const location = useLocation();
  const getClass = (path) =>
    cx("inline-block py-2 px-4 font-semibold", {
      "border-l border-t border-r rounded-t text-gray-200 bg-gray-700 border-gray-700":
        location.pathname === path,
      "text-gray-400 hover:text-gray-200": location.pathname !== path,
    });

  return (
    <div>
      <ul className="flex bg-gray-900 text-2xl">
        <li>
          <Link to="/heist" className={getClass("/heist")}>
            Heists
          </Link>
        </li>
        <li>
          <Link to="/member" className={getClass("/member")}>
            Members
          </Link>
        </li>
      </ul>
    </div>
  );
}
