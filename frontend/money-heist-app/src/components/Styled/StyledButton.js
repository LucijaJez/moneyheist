import React from "react";

const StyledButton = (props) => {
  return (
    <button
      onClick={props.onClick}
      className="mt-2 text-gray-300 bg-green-700 hover:bg-green-600 p-1 rounded"
    >
      {props.children}
    </button>
  );
};
export default StyledButton;
