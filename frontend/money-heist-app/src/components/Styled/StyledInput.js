import React from "react";

const StyledInput = (props) => {
  return (
    <input
      {...props}
      className="focus:ring text-gray-900 bg-gray-300 p-1 rounded col-span-4"
    />
  );
};

export default StyledInput;
