import { Link } from "react-router-dom";
import React from "react";

const StyledLink = (props) => {
  return (
    <Link
      className="text-gray-300 bg-green-700 hover:bg-green-600 p-1 rounded"
      to={props.to}
    >
      {props.children}
    </Link>
  );
};
export default StyledLink;
