import React, { useState, useEffect } from "react";
import * as memberApi from "../../service/memberApi";
import { useParams } from "react-router";
import Card from "../Card/Card";
import Table from "../Table/Table";

const MemberDetails = () => {
  const [member, setMember] = useState({});

  const { id } = useParams();

  useEffect(() => fetchMember(), []);

  const fetchMember = () =>
    memberApi.fetchMember(id).then((memberRes) => setMember(memberRes));

  const handleClick = (skillName) => {
    memberApi.deleteMemberSkill(id, skillName).then(fetchMember);
  };

  return (
    <>
      <Card>
        <Card.Heading>{member.name}</Card.Heading>
        <Card.Body className="grid grid-cols-2 gap-2">
          <div className="font-bold">Sex:</div>
          <div>{member.sex}</div>
          <div className="font-bold">Email:</div>
          <div>{member.email}</div>
          <div className="font-bold">Main skill:</div>
          <div>{member.mainSkill ? member.mainSkill : "-"}</div>
          <div className="font-bold">Status:</div>
          <div>{member.status}</div>
        </Card.Body>
      </Card>

      <h1 className="ml-auto mr-auto max-w-screen-md text-gray-300 text-l font-bold pt-5">
        Skills:
      </h1>

      <Table>
        <Table.Header>
          <Table.HeaderCell>Name</Table.HeaderCell>
          <Table.HeaderCell>Level</Table.HeaderCell>
        </Table.Header>
        {member.skills && (
          <Table.Body data={member.skills}>
            <Table.BodyCell itemKey="name" />
            <Table.BodyCell itemKey="level" />
            <Table.BodyCell>
              {(memberSkill) => (
                <button
                  className="bg-red-700 hover:bg-red-600 p-1 rounded"
                  onClick={() => handleClick(memberSkill.name)}
                >
                  Delete
                </button>
              )}
            </Table.BodyCell>
          </Table.Body>
        )}
      </Table>
    </>
  );
};

export default MemberDetails;
