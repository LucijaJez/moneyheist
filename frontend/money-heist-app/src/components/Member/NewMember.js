import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import * as memberApi from "../../service/memberApi";
import Card from "../Card/Card";
import StyledInput from "../Styled/StyledInput";
import Table from "../Table/Table";

const NewMember = () => {
  const [formValue, setFormValue] = useState({ status: "AVAILABLE" });

  const history = useHistory();

  const handleSubmit = (event) => {
    event.preventDefault();
    memberApi.addMember(formValue).then((location) => history.push(location));
  };

  const changeValue = (event) =>
    setFormValue({
      ...formValue,
      [event.target.name]: event.target.value,
    });

  const addSkill = (name, value) => {
    setFormValue({
      ...formValue,
      [name]: value,
    });
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <Card>
          <Card.Heading>Add new member</Card.Heading>
          <Card.Body className="grid grid-cols-6 gap-4">
            <label className="col-span-2">Name:</label>
            <StyledInput type="text" name="name" onChange={changeValue} />
            <label className="col-span-2">Sex:</label>
            <div className="col-span-4">
              <label className="pr-4">
                <input
                  type="radio"
                  value="M"
                  name="sex"
                  onClick={changeValue}
                />{" "}
                M
              </label>
              <label>
                <input
                  type="radio"
                  value="F"
                  name="sex"
                  onClick={changeValue}
                />
                F
              </label>
            </div>
            <label className="col-span-2">Email:</label>
            <StyledInput type="text" name="email" onChange={changeValue} />
            <label className="col-span-2">Main skill:</label>
            <StyledInput type="text" name="mainSkill" onChange={changeValue} />
            <label className="col-span-2">Status:</label>
            <select
              className="text-gray-900 bg-gray-300 p-1 rounded col-span-4"
              name="status"
              onLoad={changeValue}
              onChange={changeValue}
            >
              <option value="AVAILABLE">AVAILABLE</option>
              <option value="RETIRED">RETIRED</option>
              <option value="INCARCERATED">INCARCERATED</option>
              <option value="EXPIRED">EXPIRED</option>
            </select>
            <input
              className="col-span-1 text-gray-300 bg-green-700 hover:bg-green-600 p-1 rounded"
              type="submit"
              value="Submit"
            />
          </Card.Body>
        </Card>
      </form>
      <NewMemberSkill addSkill={addSkill} />
    </div>
  );
};

const NewMemberSkill = (props) => {
  const [skills, setSkills] = useState([]);
  const [formValue, setFormValue] = useState({});
  const [name, setName] = useState("");
  const [level, setLevel] = useState("");

  const changeValue = (event) => {
    setFormValue({
      ...formValue,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const updatedSkills = [...skills, formValue];
    setSkills(updatedSkills);
    props.addSkill("skills", updatedSkills);
    setName("");
    setLevel("");
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <Card>
          <Card.Heading>Add member skills</Card.Heading>
          <Card.Body className="grid grid-cols-6 gap-4">
            <label className="col-span-2">Name:</label>
            <StyledInput
              value={name}
              type="text"
              name="name"
              onChange={(e) => {
                changeValue(e);
                setName(e.target.value);
              }}
            />
            <label className="col-span-2">Level:</label>
            <StyledInput
              value={level}
              type="text"
              name="level"
              onChange={(e) => {
                changeValue(e);
                setLevel(e.target.value);
              }}
            />
            <label className="col-span-2">Required members:</label>
            <input
              className="col-span-1 text-gray-300 bg-green-700 hover:bg-green-600 p-1 rounded"
              type="submit"
              value="Add skill"
            />
          </Card.Body>
        </Card>
      </form>

      <Table>
        <Table.Header>
          <Table.HeaderCell>Name</Table.HeaderCell>
          <Table.HeaderCell>Level</Table.HeaderCell>
        </Table.Header>
        <Table.Body data={skills}>
          <Table.BodyCell itemKey="name" />
          <Table.BodyCell itemKey="level" />
        </Table.Body>
      </Table>
    </div>
  );
};

export default NewMember;
