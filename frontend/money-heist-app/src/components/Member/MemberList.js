import React, { useState, useEffect } from "react";
import Table from "../Table/Table";
import * as memberApi from "../../service/memberApi";
import StyledLink from "../Styled/StyledLink";

const MemberList = () => {
  const [members, setMembers] = useState([]);

  useEffect(() => {
    memberApi.fetchMembers().then((membersRes) => setMembers(membersRes));
  }, []);

  return (
    <>
      <StyledLink to={"member/add"}>Add member</StyledLink>

      <Table>
        <Table.Header>
          <Table.HeaderCell>Name</Table.HeaderCell>
          <Table.HeaderCell>Sex</Table.HeaderCell>
          <Table.HeaderCell>Email</Table.HeaderCell>
          <Table.HeaderCell>Main skill</Table.HeaderCell>
          <Table.HeaderCell>Status</Table.HeaderCell>
          <Table.HeaderCell />
        </Table.Header>
        <Table.Body data={members}>
          <Table.BodyCell itemKey="name" />
          <Table.BodyCell itemKey="sex" />
          <Table.BodyCell itemKey="email" />
          <Table.BodyCell>
            {(member) => (member.mainSkill ? member.mainSkill : "-")}
          </Table.BodyCell>
          <Table.BodyCell itemKey="status" />
          <Table.BodyCell>
            {(member) => (
              <StyledLink to={"member/" + member.id}>Details</StyledLink>
            )}
          </Table.BodyCell>
        </Table.Body>
      </Table>
    </>
  );
};

export default MemberList;
