import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import HeistList from "../Heist/HeistList";
import MemberList from "../Member/MemberList";
import HeistDetails from "../Heist/HeistDetails";
import MemberDetails from "../Member/MemberDetails";
import NewMember from "../Member/NewMember";
import NewHeist from "../Heist/NewHeist";

export default function RouterSwitch() {
  return (
    <Switch>
      <Route exact path="/member/add">
        <NewMember />
      </Route>
      <Route exact path="/heist/add">
        <NewHeist />
      </Route>
      <Route exact path="/heist/:id">
        <HeistDetails />
      </Route>
      <Route exact path="/heist">
        <HeistList />
      </Route>
      <Route exact path="/member/:id">
        <MemberDetails />
      </Route>
      <Route exact path="/member">
        <MemberList />
      </Route>
      <Redirect from="/" to="/heist" />
    </Switch>
  );
}
