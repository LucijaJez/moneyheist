import React from "react";

function Layout(props) {
  return <div className="bg-gray-700 min-h-screen">{props.children}</div>;
}

function LayoutHeader(props) {
  return <div className="mb-4 sticky">{props.children}</div>;
}

function LayoutBody(props) {
  return (
    <div className="ml-auto mr-auto max-w-screen-md ">{props.children}</div>
  );
}

function LayoutFooter(props) {
  return <div className="p-4">{props.children}</div>;
}

Layout.Header = LayoutHeader;
Layout.Body = LayoutBody;
Layout.Footer = LayoutFooter;

export default Layout;
