import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Navigation from "./Navigation/Navigation";
import RouterSwitch from "./RouterSwitch/RouterSwitch";
import Layout from "./Layout/Layout";

export default function App() {
  return (
    <Router>
      <Layout>
        <Layout.Header>
          <Navigation />
        </Layout.Header>
        <Layout.Body>
          <RouterSwitch />
        </Layout.Body>
        <Layout.Footer />
      </Layout>
    </Router>
  );
}
