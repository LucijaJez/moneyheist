import React from "react";

const RowContext = React.createContext({});

const Table = (props) => {
  return (
    <div className="pt-5 ">
      <table className="text-gray-300 bg-gray-800 rounded min-w-768px">
        {props.children}
      </table>
    </div>
  );
};

function TableHeader(props) {
  return (
    <thead>
      <tr>{props.children}</tr>
    </thead>
  );
}

function HeaderCell(props) {
  return <th>{props.children}</th>;
}

function TableBody(props) {
  return (
    <tbody>
      {props.data.map((row) => (
        <tr>
          <RowContext.Provider value={row}>
            {props.children}
          </RowContext.Provider>
        </tr>
      ))}
    </tbody>
  );
}

function BodyCell(props) {
  const row = React.useContext(RowContext);
  if (props.itemKey) {
    return <td className="text-center py-2 px-6">{row[props.itemKey]}</td>;
  }
  return <td className="text-center py-2 px-6">{props.children(row)}</td>;
}

Table.Header = TableHeader;
Table.HeaderCell = HeaderCell;
Table.Body = TableBody;
Table.BodyCell = BodyCell;

export default Table;
