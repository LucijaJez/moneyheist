import React from "react";

const Card = (props) => {
  return <div className="text-gray-300 min-w-768px ">{props.children}</div>;
};

const CardHeading = (props) => {
  return <div className="text-xl font-bold pt-5">{props.children}</div>;
};

const CardBody = (props) => {
  return (
    <div className={"bg-gray-800 rounded mt-5 p-5 " + props.className}>
      {props.children}
    </div>
  );
};

Card.Heading = CardHeading;
Card.Body = CardBody;

export default Card;
