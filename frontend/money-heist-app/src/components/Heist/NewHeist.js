import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import * as heistApi from "../../service/heistApi";
import Card from "../Card/Card";
import StyledInput from "../Styled/StyledInput";
import Table from "../Table/Table";

const NewHeist = () => {
  const [formValue, setFormValue] = useState({});

  const history = useHistory();

  const handleSubmit = (event) => {
    event.preventDefault();
    heistApi.addHeist(formValue).then((location) => history.push(location));
  };

  const changeValue = (event) => {
    setFormValue({
      ...formValue,
      [event.target.name]: event.target.value,
    });
  };

  const addSkill = (name, value) => {
    setFormValue({
      ...formValue,
      [name]: value,
    });
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <Card>
          <Card.Heading>Add new heist</Card.Heading>
          <Card.Body className="grid grid-cols-6 gap-4">
            <label className="col-span-2">Name:</label>
            <StyledInput type="text" name="name" onChange={changeValue} />
            <label className="col-span-2">Location:</label>
            <StyledInput type="text" name="location" onChange={changeValue} />
            <label className="col-span-2">Start time:</label>
            <StyledInput
              type="datetime-local"
              name="startTime"
              onChange={changeValue}
            />
            <label className="col-span-2">End time:</label>
            {/*not working in firefox, works in chrome*/}
            {/*in text input should be as 2021-08-07T00:00*/}
            <StyledInput
              type="datetime-local"
              name="endTime"
              onChange={changeValue}
            />
            <input
              className="col-span-1 text-gray-300 bg-green-700 hover:bg-green-600 p-1 rounded"
              type="submit"
              value="Submit"
            />
          </Card.Body>
        </Card>
      </form>
      <NewHeistSkill addSkill={addSkill} />
    </div>
  );
};

const NewHeistSkill = (props) => {
  const [skills, setSkills] = useState([]);
  const [formValue, setFormValue] = useState({});
  const [name, setName] = useState("");
  const [level, setLevel] = useState("");
  const [members, setMembers] = useState("");

  const changeValue = (event) => {
    setFormValue({
      ...formValue,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const updatedSkills = [...skills, formValue];
    setSkills(updatedSkills);
    props.addSkill("skills", updatedSkills);
    setName("");
    setLevel("");
    setMembers("");
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <Card>
          <Card.Heading>Add required skills for the heist</Card.Heading>
          <Card.Body className="grid grid-cols-6 gap-4">
            <label className="col-span-2">Name:</label>
            <StyledInput
              value={name}
              type="text"
              name="name"
              onChange={(e) => {
                changeValue(e);
                setName(e.target.value);
              }}
            />
            <label className="col-span-2">Level:</label>
            <StyledInput
              value={level}
              type="text"
              name="level"
              onChange={(e) => {
                changeValue(e);
                setLevel(e.target.value);
              }}
            />
            <label className="col-span-2">Required members:</label>
            <StyledInput
              value={members}
              type="text"
              name="members"
              onChange={(e) => {
                changeValue(e);
                setMembers(e.target.value);
              }}
            />
            <input
              className="col-span-1 text-gray-300 bg-green-700 hover:bg-green-600 p-1 rounded"
              type="submit"
              value="Add skill"
            />
          </Card.Body>
        </Card>
      </form>

      <Table>
        <Table.Header>
          <Table.HeaderCell>Name</Table.HeaderCell>
          <Table.HeaderCell>Level</Table.HeaderCell>
          <Table.HeaderCell>Members</Table.HeaderCell>
        </Table.Header>
        <Table.Body data={skills}>
          <Table.BodyCell itemKey="name" />
          <Table.BodyCell itemKey="level" />
          <Table.BodyCell itemKey="members" />
        </Table.Body>
      </Table>
    </div>
  );
};

export default NewHeist;
