import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import * as heistApi from "../../service/heistApi";
import Card from "../Card/Card";
import Table from "../Table/Table";
import StyledButton from "../Styled/StyledButton";

const HeistDetails = () => {
  const [heist, setHeist] = useState({});
  const { id } = useParams();

  const fetchHeistAndOutcome = async () => {
    const heist = await heistApi.fetchHeist(id);
    if (heist.status === "FINISHED") {
      const outcome = await heistApi.fetchHeistOutcome(id);
      setHeist({ ...heist, ...outcome });
    } else {
      setHeist({ ...heist });
    }
  };

  const heistStart = () => {
    heistApi.heistStart(id).then(fetchHeistAndOutcome);
  };

  useEffect(() => {
    (async () => {
      await fetchHeistAndOutcome();
    })();
  }, []);

  return (
    <>
      <Card>
        <Card.Heading>{heist.name}</Card.Heading>
        <Card.Body className="grid grid-cols-2 gap-2">
          <div className="font-bold">Location:</div>
          <div>{heist.location}</div>
          <div className="font-bold">Start time:</div>
          <div>{new Date(heist.startTime).toLocaleString()}</div>
          <div className="font-bold">End time:</div>
          <div>{new Date(heist.endTime).toLocaleString()}</div>
          <div className="font-bold">Status:</div>
          <div>{heist.status}</div>
          <div className="font-bold">Outcome:</div>
          <div>{heist.outcome ? heist.outcome : "-"}</div>
        </Card.Body>
      </Card>

      {heist.status === "READY" && (
        <StyledButton onClick={heistStart}>START</StyledButton>
      )}

      <h1 className="text-gray-300 text-l font-bold pt-5">Required skills:</h1>

      <Table>
        <Table.Header>
          <Table.HeaderCell>Name</Table.HeaderCell>
          <Table.HeaderCell>Level</Table.HeaderCell>
          <Table.HeaderCell>Members</Table.HeaderCell>
        </Table.Header>
        {heist.skills && (
          <Table.Body data={heist.skills}>
            <Table.BodyCell itemKey="name" />
            <Table.BodyCell itemKey="level" />
            <Table.BodyCell itemKey="members" />
          </Table.Body>
        )}
      </Table>
      {heist.status === "PLANNING" && (
        <EligibleMembers id={id} rerender={fetchHeistAndOutcome} />
      )}
      {(heist.status === "FINISHED" ||
        heist.status === "IN_PROGRESS" ||
        heist.status === "READY") && <HeistMembers id={id} />}
    </>
  );
};

const EligibleMembers = (props) => {
  const [eligibleMembers, setEligibleMembers] = useState([]);
  const [membersToConfirm, setMembersToConfirm] = useState([]);

  useEffect(() => {
    heistApi.fetchHeistEligibleMembers(props.id).then((eligibleMembersRes) => {
      setEligibleMembers(eligibleMembersRes.members);
    });
  }, []);

  const handleChange = (e) => {
    if (e.target.checked) {
      setMembersToConfirm([...membersToConfirm, e.target.value]);
    } else {
      setMembersToConfirm(
        membersToConfirm.filter(
          (memberToConfirm) => memberToConfirm !== e.target.value
        )
      );
    }
  };

  const confirmMembers = () => {
    const membersObject = { members: membersToConfirm };
    heistApi.confirmHeistMembers(props.id, membersObject).then(props.rerender);
  };

  return (
    <div>
      <h1 className="text-gray-300 text-l font-bold pt-5">Eligible members:</h1>

      <Table>
        <Table.Header>
          <Table.HeaderCell>Name</Table.HeaderCell>
        </Table.Header>
        <Table.Body data={eligibleMembers}>
          <Table.BodyCell itemKey="name" />
          <Table.BodyCell>
            {(eligibleMember) => (
              <input
                className="form-checkbox h-5 w-5"
                name={eligibleMember.name}
                value={eligibleMember.name}
                type="checkbox"
                onChange={handleChange}
              />
            )}
          </Table.BodyCell>
        </Table.Body>
      </Table>
      <StyledButton onClick={confirmMembers}>Confirm members</StyledButton>
    </div>
  );
};

const HeistMembers = (props) => {
  const [heistMembers, setHeistMembers] = useState([]);

  useEffect(() => {
    heistApi.fetchHeistMembers(props.id).then((heistMembersRes) => {
      setHeistMembers(heistMembersRes);
    });
  }, []);

  return (
    <div>
      <h1 className="text-gray-300 text-l font-bold pt-5">Members:</h1>

      <Table>
        <Table.Header>
          <Table.HeaderCell>Name</Table.HeaderCell>
        </Table.Header>
        <Table.Body data={heistMembers}>
          <Table.BodyCell itemKey="name" />
        </Table.Body>
      </Table>
    </div>
  );
};

export default HeistDetails;
