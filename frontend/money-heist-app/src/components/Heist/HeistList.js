import React, { useState, useEffect } from "react";
import Table from "../Table/Table";
import * as heistApi from "../../service/heistApi";
import StyledLink from "../Styled/StyledLink";

const HeistList = () => {
  const [heists, setHeists] = useState([]);

  useEffect(() => {
    heistApi.fetchHeists().then((heistsRes) => setHeists(heistsRes));
  }, []);

  return (
    <>
      <StyledLink to={"heist/add"}>Add heist</StyledLink>

      <Table>
        <Table.Header>
          <Table.HeaderCell>Name</Table.HeaderCell>
          <Table.HeaderCell>Location</Table.HeaderCell>
          <Table.HeaderCell>Start time</Table.HeaderCell>
          <Table.HeaderCell>End time</Table.HeaderCell>
          <Table.HeaderCell>Status</Table.HeaderCell>
        </Table.Header>
        <Table.Body data={heists}>
          <Table.BodyCell itemKey="name" />
          <Table.BodyCell itemKey="location" />
          <Table.BodyCell>
            {(heist) => new Date(heist.startTime).toLocaleString()}
          </Table.BodyCell>
          <Table.BodyCell>
            {(heist) => new Date(heist.endTime).toLocaleString()}
          </Table.BodyCell>
          <Table.BodyCell itemKey="status" />
          <Table.BodyCell>
            {(heist) => (
              <StyledLink to={"heist/" + heist.id}>Details</StyledLink>
            )}
          </Table.BodyCell>
        </Table.Body>
      </Table>
    </>
  );
};

export default HeistList;
