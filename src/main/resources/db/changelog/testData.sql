INSERT INTO skill (name)
VALUES ('combat'), ('driving'), ('mining'), ('lockpicking'), ('coding');

INSERT INTO member (name, sex, email, main_skill, status)
VALUES ('Helsinki', 'M', 'helsinki@ag04.com', (SELECT id FROM skill WHERE name='combat'), 'AVAILABLE'),
('a', 'F', 'a@ag04.com', (SELECT id FROM skill WHERE name='driving'), 'AVAILABLE'),
('b', 'M', 'b@ag04.com', (SELECT id FROM skill WHERE name='mining'), 'AVAILABLE'),
('c', 'F', 'c@ag04.com', (SELECT id FROM skill WHERE name='lockpicking'), 'RETIRED'),
('d', 'M', 'd@ag04.com', (SELECT id FROM skill WHERE name='coding'), 'RETIRED'),
('e', 'F', 'e@ag04.com', (SELECT id FROM skill WHERE name='combat'), 'INCARCERATED'),
('f', 'M', 'f@ag04.com', (SELECT id FROM skill WHERE name='combat'), 'AVAILABLE'),
('g', 'F', 'g@ag04.com', (SELECT id FROM skill WHERE name='combat'), 'AVAILABLE'),
('h', 'M', 'h@ag04.com', NULL, 'AVAILABLE'),
('i', 'F', 'i@ag04.com', NULL, 'AVAILABLE');


INSERT INTO member_skill (member_id, skill_id, level)
VALUES
(1, (SELECT id FROM skill WHERE name='combat'), '**********'),
(1, (SELECT id FROM skill WHERE name='driving'), '****'),
(1, (SELECT id FROM skill WHERE name='mining'), '*****'),
(1, (SELECT id FROM skill WHERE name='lockpicking'), '**'),

(2, (SELECT id FROM skill WHERE name='combat'), '**'),
(2, (SELECT id FROM skill WHERE name='driving'), '****'),

(3, (SELECT id FROM skill WHERE name='combat'), '**********'),
(3, (SELECT id FROM skill WHERE name='mining'), '**********'),

(4, (SELECT id FROM skill WHERE name='lockpicking'), '**********'),
(4, (SELECT id FROM skill WHERE name='driving'), '****'),

(5, (SELECT id FROM skill WHERE name='coding'), '******'),

(6, (SELECT id FROM skill WHERE name='combat'), '****'),
(6, (SELECT id FROM skill WHERE name='driving'), '**'),

(7, (SELECT id FROM skill WHERE name='combat'), '*****'),
(7, (SELECT id FROM skill WHERE name='driving'), '**'),
(7, (SELECT id FROM skill WHERE name='lockpicking'), '*'),
(7, (SELECT id FROM skill WHERE name='coding'), '****'),

(8, (SELECT id FROM skill WHERE name='combat'), '********'),
(8, (SELECT id FROM skill WHERE name='driving'), '*****'),

(9, (SELECT id FROM skill WHERE name='mining'), '*****'),

(10, (SELECT id FROM skill WHERE name='coding'), '******');
