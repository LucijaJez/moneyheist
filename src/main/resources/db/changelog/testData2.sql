INSERT INTO heist (name, location, start_time, end_time)
VALUES ('Fábrica Nacional de Moneda y Timbre', 'Spain', '2020-09-05T22:00:00.000Z', '2020-09-10T18:00:00.000Z'),
('Museo', 'Italy', '2021-09-05T22:00:00.000Z', '2021-09-10T18:00:00.000Z'),
('Toy Store', 'London', '2022-09-05T22:00:00.000Z', '2022-09-10T18:00:00.000Z'),
('Bank', 'Mexico', '2023-09-05T22:00:00.000Z', '2023-09-10T18:00:00.000Z');

INSERT INTO heist_skill (heist_id, skill_id, level, members)
VALUES (1, (SELECT id FROM skill WHERE name='combat'), '*****', 1),
(1, (SELECT id FROM skill WHERE name='combat'), '*', 3),

(2, (SELECT id FROM skill WHERE name='driving'), '*****', 3),

(3, (SELECT id FROM skill WHERE name='mining'), '*', 1),


(4, (SELECT id FROM skill WHERE name='combat'), '***', 3),
(4, (SELECT id FROM skill WHERE name='lockpicking'), '*', 1),
(4, (SELECT id FROM skill WHERE name='coding'), '*', 2);
