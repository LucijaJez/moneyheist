package org.agency04.moneyheist.tasks;

import org.agency04.moneyheist.service.HeistService;

public class EndHeistRunnable implements Runnable {

    Integer heistId;
    HeistService heistService;

    public EndHeistRunnable(Integer heistId, HeistService heistService) {
        this.heistId = heistId;
        this.heistService = heistService;
    }

    @Override
    public void run() {
        try {
            System.out.println("End " + heistId);
            heistService.end(heistId);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
