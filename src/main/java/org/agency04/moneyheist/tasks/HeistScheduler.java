package org.agency04.moneyheist.tasks;

import org.agency04.moneyheist.entities.Heist;
import org.agency04.moneyheist.service.HeistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;


@Component
public class HeistScheduler {

    TaskScheduler taskScheduler;
    HeistService heistService;

    @Autowired
    public HeistScheduler(TaskScheduler taskScheduler, HeistService heistService) {
        this.taskScheduler = taskScheduler;
        this.heistService = heistService;
    }


    //    every 1 minute check for heists that start / end in next 1 minute interval
    @Scheduled(fixedRate = 60000)
    public void heistsToRun() {
        Timestamp before = Timestamp.valueOf(LocalDateTime.now().plusMinutes(1));
        List<Heist> startingHeists = heistService.findByStartTimeBefore(before);
        List<Heist> endingHeists = heistService.findByEndTimeBefore(before);
        for (Heist startingHeist : startingHeists) {
            taskScheduler.schedule(new StartHeistRunnable(startingHeist.getId(), heistService), startingHeist.getStartTime());
        }
        for (Heist endingHeist : endingHeists) {
            taskScheduler.schedule(new EndHeistRunnable(endingHeist.getId(), heistService), endingHeist.getEndTime());
        }
    }

}

