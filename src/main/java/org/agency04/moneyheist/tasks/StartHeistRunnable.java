package org.agency04.moneyheist.tasks;

import org.agency04.moneyheist.service.HeistService;

public class StartHeistRunnable implements Runnable {

    Integer heistId;
    HeistService heistService;

    public StartHeistRunnable(Integer heistId, HeistService heistService) {
        this.heistId = heistId;
        this.heistService = heistService;
    }

    @Override
    public void run() {
        try {
            System.out.println("Start " + heistId);
            heistService.start(heistId);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
