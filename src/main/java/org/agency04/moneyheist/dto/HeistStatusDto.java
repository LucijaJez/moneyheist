package org.agency04.moneyheist.dto;

import org.agency04.moneyheist.entities.HeistStatus;

public class HeistStatusDto {

    private HeistStatus status;


    public HeistStatusDto() {
    }

    public HeistStatusDto(HeistStatus status) {
        this.status = status;
    }

    public HeistStatus getStatus() {
        return status;
    }

    public void setStatus(HeistStatus status) {
        this.status = status;
    }
}
