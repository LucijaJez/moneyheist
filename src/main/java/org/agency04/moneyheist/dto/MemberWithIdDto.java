package org.agency04.moneyheist.dto;

import org.agency04.moneyheist.entities.Sex;

import java.util.ArrayList;
import java.util.List;

public class MemberWithIdDto {

    private Integer id;
    private String name;
    private Sex sex;
    private String email;
    private List<MemberSkillDto> skills = new ArrayList<>();
    private String mainSkill;
    private String status;

    public MemberWithIdDto() {
    }

    public MemberWithIdDto(Integer id, String name, Sex sex, String email, List<MemberSkillDto> skills, String mainSkill, String status) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.email = email;
        this.skills = skills;
        this.mainSkill = mainSkill;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<MemberSkillDto> getSkills() {
        return skills;
    }

    public void setSkills(List<MemberSkillDto> skills) {
        this.skills = skills;
    }

    public String getMainSkill() {
        return mainSkill;
    }

    public void setMainSkill(String mainSkill) {
        this.mainSkill = mainSkill;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
