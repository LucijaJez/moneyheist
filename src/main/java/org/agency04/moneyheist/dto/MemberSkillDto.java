package org.agency04.moneyheist.dto;

public class MemberSkillDto {

    private String name;
    private String level;

    public MemberSkillDto() {
    }

    public MemberSkillDto(String name, String level) {
        this.name = name;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
