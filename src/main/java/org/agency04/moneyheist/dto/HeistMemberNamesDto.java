package org.agency04.moneyheist.dto;

import java.util.List;

public class HeistMemberNamesDto {

    private List<String> members;

    public HeistMemberNamesDto() {
    }

    public HeistMemberNamesDto(List<String> members) {
        this.members = members;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }
}
