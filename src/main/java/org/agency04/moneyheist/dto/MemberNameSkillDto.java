package org.agency04.moneyheist.dto;

import java.util.ArrayList;
import java.util.List;

public class MemberNameSkillDto {

    private String name;
    private List<MemberSkillDto> skills = new ArrayList<>();


    public MemberNameSkillDto() {
    }

    public MemberNameSkillDto(String name, List<MemberSkillDto> skills) {
        this.name = name;
        this.skills = skills;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MemberSkillDto> getSkills() {
        return skills;
    }

    public void setSkills(List<MemberSkillDto> skills) {
        this.skills = skills;
    }
}
