package org.agency04.moneyheist.dto;

import java.util.ArrayList;
import java.util.List;

public class UpdatedSkillsDto {

    private List<HeistSkillDto> skills = new ArrayList<>();

    public UpdatedSkillsDto() {
    }

    public UpdatedSkillsDto(List<HeistSkillDto> skills) {
        this.skills = skills;
    }

    public List<HeistSkillDto> getSkills() {
        return skills;
    }

    public void setSkills(List<HeistSkillDto> skills) {
        this.skills = skills;
    }
}

