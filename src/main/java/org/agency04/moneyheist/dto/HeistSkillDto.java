package org.agency04.moneyheist.dto;

public class HeistSkillDto {

    private String name;
    private String level;
    private Integer members;

    public HeistSkillDto() {
    }

    public HeistSkillDto(String name, String level, Integer members) {
        this.name = name;
        this.level = level;
        this.members = members;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Integer getMembers() {
        return members;
    }

    public void setMembers(Integer members) {
        this.members = members;
    }
}
