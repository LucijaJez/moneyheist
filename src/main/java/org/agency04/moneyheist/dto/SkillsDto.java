package org.agency04.moneyheist.dto;

import java.util.ArrayList;
import java.util.List;

public class SkillsDto {

    private List<MemberSkillDto> skills = new ArrayList<>();
    private String mainSkill;

    public SkillsDto() {
    }

    public SkillsDto(List<MemberSkillDto> skills, String mainSkill) {
        this.skills = skills;
        this.mainSkill = mainSkill;
    }

    public List<MemberSkillDto> getSkills() {
        return skills;
    }

    public void setSkills(List<MemberSkillDto> skills) {
        this.skills = skills;
    }

    public String getMainSkill() {
        return mainSkill;
    }

    public void setMainSkill(String mainSkill) {
        this.mainSkill = mainSkill;
    }
}

