package org.agency04.moneyheist.dto;

import java.util.List;

public class EligibleMembersDto {

    private List<HeistSkillDto> skills;
    private List<MemberDto> members;

    public EligibleMembersDto() {
    }

    public EligibleMembersDto(List<HeistSkillDto> skills, List<MemberDto> members) {
        this.skills = skills;
        this.members = members;
    }

    public List<HeistSkillDto> getSkills() {
        return skills;
    }

    public void setSkills(List<HeistSkillDto> skills) {
        this.skills = skills;
    }

    public List<MemberDto> getMembers() {
        return members;
    }

    public void setMembers(List<MemberDto> members) {
        this.members = members;
    }
}
