package org.agency04.moneyheist.dto;

import org.agency04.moneyheist.entities.Outcome;


public class OutcomeDto {

    private Outcome outcome;

    public OutcomeDto() {
    }

    public OutcomeDto(Outcome outcome) {
        this.outcome = outcome;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public void setOutcome(Outcome outcome) {
        this.outcome = outcome;
    }
}

