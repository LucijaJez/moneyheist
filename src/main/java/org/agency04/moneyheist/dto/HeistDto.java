package org.agency04.moneyheist.dto;

import org.agency04.moneyheist.entities.HeistStatus;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class HeistDto {

    private String name;
    private String location;
    private Timestamp startTime;
    private Timestamp endTime;
    private List<HeistSkillDto> skills = new ArrayList<>();
    private HeistStatus status = HeistStatus.PLANNING;


    public HeistDto() {
    }


    public HeistDto(String name, String location, Timestamp startTime, Timestamp endTime, List<HeistSkillDto> skills, HeistStatus status) {
        this.name = name;
        this.location = location;
        this.startTime = startTime;
        this.endTime = endTime;
        this.skills = skills;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public List<HeistSkillDto> getSkills() {
        return skills;
    }

    public void setSkills(List<HeistSkillDto> skills) {
        this.skills = skills;
    }

    public HeistStatus getStatus() {
        return status;
    }

    public void setStatus(HeistStatus status) {
        this.status = status;
    }
}
