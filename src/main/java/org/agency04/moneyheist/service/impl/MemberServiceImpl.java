package org.agency04.moneyheist.service.impl;

import org.agency04.moneyheist.dto.MemberSkillDto;
import org.agency04.moneyheist.dto.SkillsDto;
import org.agency04.moneyheist.entities.*;
import org.agency04.moneyheist.repository.MemberRepository;
import org.agency04.moneyheist.service.EmailService;
import org.agency04.moneyheist.service.MemberService;
import org.agency04.moneyheist.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MemberServiceImpl implements MemberService {

    private final MemberRepository memberRepository;
    private final SkillService skillService;
    private final EmailService emailService;

    @Autowired
    public MemberServiceImpl(MemberRepository memberRepository, SkillService skillService, EmailService emailService) {
        this.memberRepository = memberRepository;
        this.skillService = skillService;
        this.emailService = emailService;
    }


    @Override
    public List<Member> findAll() {
        return memberRepository.findAll();
    }


    //  sbss-09 - 1.
    @Override
    public Member findById(int memberId) {
        final Optional<Member> memberOptional = memberRepository.findById(memberId);
        if (memberOptional.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return memberOptional.get();
    }


    @Override
    public Member findByName(String name) {
        final Optional<Member> memberOptional = memberRepository.findByName(name);
        if (memberOptional.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return memberOptional.get();
    }


    //  sbss-01 and sbss-12
    @Override
    @Transactional
    public Member addMember(Member member) {
        if (memberRepository.existsByEmail(member.getEmail()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        emailService.sendEmail(member.getEmail(), "membership", "You have been added as a member!");
        return memberRepository.save(member);
    }


    // sbss-02
    @Override
    @Transactional
    public void updateSkills(int memberId, SkillsDto skillsDto) {
//      find member if exists
        final Member member = findById(memberId);

        final String newMainSkillName = skillsDto.getMainSkill();
        final String oldMainSkillName = member.getMainSkill().getName();
        final List<MemberSkill> oldSkills = member.getMemberSkills();
        final List<String> oldSkillsNames = oldSkills.stream()
                .map(oldSkill -> oldSkill.getSkill().getName())
                .collect(Collectors.toList());
        final List<MemberSkillDto> newSkillsDto = skillsDto.getSkills();

//      main skill not in old or new skills
        if (newMainSkillName != null) {
            final boolean containsPrevious = oldSkillsNames.contains(newMainSkillName);
            final boolean containsUpdated = newSkillsDto.stream()
                    .map(skillDto -> skillDto.getName())
                    .collect(Collectors.toList()).contains(newMainSkillName);
            if (!containsPrevious && !containsUpdated)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
//      skills have the same name
        final long size = newSkillsDto.size();
        final long sizeDistinct = newSkillsDto.stream()
                .map(skillDto -> skillDto.getName())
                .distinct().count();
        if (size != sizeDistinct)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

//      update existing skill level
        for (MemberSkillDto newSkillDto : newSkillsDto) {
            for (MemberSkill oldSkill : oldSkills) {
                if (Objects.equals(oldSkill.getSkill().getName(), newSkillDto.getName()))
                    oldSkill.setLevel(newSkillDto.getLevel());
            }
        }

//      new skill not in old skills, add member skill
        for (MemberSkillDto newSkillDto : newSkillsDto) {
            final boolean newSkillInOldSkills = oldSkillsNames.contains(newSkillDto.getName());
            if (!newSkillInOldSkills) {
                Skill skill = skillService.findByName(newSkillDto.getName());
//              new skill is new in database, add skill
                if (skill == null)
                    skill = skillService.addSkill(new Skill(newSkillDto.getName()));
                MemberSkill memberSkill = new MemberSkill();
                memberSkill.setMemberSkillId(new MemberSkillId());
                memberSkill.setLevel(newSkillDto.getLevel());
                memberSkill.setMember(member);
                memberSkill.setSkill(skill);
                oldSkills.add(memberSkill);
            }
        }

//      main skill has changed, set new main skill value
        if (newMainSkillName != null && !Objects.equals(newMainSkillName, oldMainSkillName)) {
            member.setMainSkill(skillService.findByName(newMainSkillName));
        }

        memberRepository.save(member);
    }


    //    sbss-03
    @Override
    @Transactional
    public void deleteSkill(int memberId, String skillName) {
//      find member if exists
        final Member member = findById(memberId);
        final List<MemberSkill> memberSkills = member.getMemberSkills();
        final List<String> memberSkillsNames = memberSkills.stream()
                .map(memberSkill -> memberSkill.getSkill().getName())
                .collect(Collectors.toList());

//      member skill not found
        if (!memberSkillsNames.contains(skillName))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

//      if deleted skill is also a main skill
        if (member.getMainSkill() != null && Objects.equals(skillName, member.getMainSkill().getName()))
            member.setMainSkill(null);

//      delete member skill
        final int skillId = skillService.findByName(skillName).getId();
        memberSkills.removeIf(memberSkill -> Objects
                .equals(memberSkill.getSkill().getName(), skillName));
        memberRepository.save(member);
    }


    @Override
    public Member saveMember(Member member) {
        return memberRepository.save(member);
    }
}
