package org.agency04.moneyheist.service.impl;

import org.agency04.moneyheist.entities.Skill;
import org.agency04.moneyheist.repository.SkillRepository;
import org.agency04.moneyheist.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
public class SkillServiceImpl implements SkillService {

    private SkillRepository skillRepository;

    @Autowired
    public SkillServiceImpl(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    @Override
    public Skill findByName(String name) {
        return skillRepository.findByName((name == null) ? null : name.toLowerCase());
    }

    @Override
    @Transactional
    public Skill addSkill(Skill skill) {
        return skillRepository.save(skill);
    }
}
