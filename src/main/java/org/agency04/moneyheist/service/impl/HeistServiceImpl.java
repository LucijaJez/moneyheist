package org.agency04.moneyheist.service.impl;

import org.agency04.moneyheist.config.MemberSkillProperties;
import org.agency04.moneyheist.dto.HeistSkillDto;
import org.agency04.moneyheist.entities.*;
import org.agency04.moneyheist.repository.HeistRepository;
import org.agency04.moneyheist.service.EmailService;
import org.agency04.moneyheist.service.HeistService;
import org.agency04.moneyheist.service.MemberService;
import org.agency04.moneyheist.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class HeistServiceImpl implements HeistService {

    private final HeistRepository heistRepository;
    private final SkillService skillService;
    private final MemberService memberService;
    private final EmailService emailService;
    private final MemberSkillProperties memberSkillProperties;

    @Autowired
    public HeistServiceImpl(HeistRepository heistRepository, SkillService skillService, MemberService memberService, EmailService emailService, MemberSkillProperties memberSkillProperties) {
        this.heistRepository = heistRepository;
        this.skillService = skillService;
        this.memberService = memberService;
        this.emailService = emailService;
        this.memberSkillProperties = memberSkillProperties;
    }


    @Override
    public List<Heist> findAll() {
        return heistRepository.findAll();
    }


    //  sbss-09 - 3.
    @Override
    public Heist findById(int heistId) {
        final Optional<Heist> heistOptional = heistRepository.findById(heistId);
        if (heistOptional.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return heistOptional.get();
    }


    //  sbss-09 - 4.
    @Override
    public List<Member> getHeistMembers(int heistId) {
        final Heist heist = findById(heistId);
        if (heist.getStatus() == HeistStatus.PLANNING)
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED);
        return heist.getMembers();
    }


    //  sbss-09 - 5.
    @Override
    public List<HeistSkill> getHeistSkills(int heistId) {
        final Heist heist = findById(heistId);
        return heist.getHeistSkills();
    }


    //    sbss-04
    @Override
    @Transactional
    public Heist addHeist(Heist heist) {
        if (heistRepository.existsByName(heist.getName()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if (heist.getStartTime().after(heist.getEndTime()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if (heist.getEndTime().before(new Timestamp(System.currentTimeMillis())))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        return heistRepository.save(heist);
    }


    //    sbss-05
    @Override
    @Transactional
    public void updateSkills(int heistId, List<HeistSkillDto> heistSkillsDto) {
//      find heist if exists
        final Heist heist = findById(heistId);
        final List<HeistSkill> oldHeistSkills = heist.getHeistSkills();

//      heist has already started
        if (heist.getStatus() == HeistStatus.IN_PROGRESS || heist.getStatus() == HeistStatus.FINISHED)
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED);

//      multiple skills with the same name and level provided
        final long size = heistSkillsDto.size();
        final long sizeDistinct = heistSkillsDto.stream()
                .map(heistSkillDto -> Pair.of(heistSkillDto.getName(), heistSkillDto.getLevel()))
                .distinct().count();
        if (size != sizeDistinct)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

//      update members for existing (skillName,level) pair for a heist
        final Map<Pair<String, String>, HeistSkill> oldHeistSkillMap = oldHeistSkills.stream()
                .collect(Collectors.toMap(
                        heistSkill -> Pair.of(heistSkill.getSkill().getName(), heistSkill.getHeistSkillId().getLevel()),
                        heistSkill -> heistSkill
                ));
        final Set<HeistSkillDto> setToAdd = new HashSet<>();

        for (HeistSkillDto heistSkillDto : heistSkillsDto) {
            final Pair<String, String> nameLevelPair = Pair.of(heistSkillDto.getName(), heistSkillDto.getLevel());
            if (oldHeistSkillMap.containsKey(nameLevelPair)) {
                oldHeistSkillMap.get(nameLevelPair).setMembers(heistSkillDto.getMembers());
            } else {
                setToAdd.add(heistSkillDto);
            }
        }

        final Map<String, Skill> newSkills = new HashMap<>();

//      new skill name and level not in old skills, add heist skill
        for (HeistSkillDto toAdd : setToAdd) {
            final String newSkillName = toAdd.getName();
            Skill skill = skillService.findByName(newSkillName);

//          new skill is new in database, add skill
            if (skill == null) {
//          if multiple new skills with the same name were provided, save new skill only once
                if (newSkills.containsKey(newSkillName)) {
                    skill = newSkills.get(newSkillName);
                } else {
                    skill = skillService.addSkill(new Skill(newSkillName));
                    newSkills.put(newSkillName, skill);
                }
            }
            HeistSkill heistSkill = new HeistSkill();
            heistSkill.setHeistSkillId(new HeistSkillId());
            heistSkill.getHeistSkillId().setLevel(toAdd.getLevel());
            heistSkill.setMembers(toAdd.getMembers());
            heistSkill.setHeist(heist);
            heistSkill.setSkill(skill);
            oldHeistSkills.add(heistSkill);
        }

        heistRepository.save(heist);
    }


    //  sbss-06
    @Override
    public List<Member> findEligibleMembers(int heistId) {

        Heist heist = findById(heistId);
        if (heist.getStatus() != HeistStatus.PLANNING) {
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED);
        }

        final List<Member> eligibleMembers = heistRepository.findEligibleMembers(heistId);
        Iterator<Member> it = eligibleMembers.iterator();

        // member is not eligible if participates in another heist that's in the same time window as heist
        while (it.hasNext()) {
            Member nextMember = it.next();
            for (Heist h : nextMember.getHeists()) {
                if (!(h.getEndTime().before(heist.getStartTime()) || h.getStartTime().after(heist.getEndTime()))) {
                    it.remove();
                    break;
                }
            }
        }
        return eligibleMembers;
    }


    //    sbss-07 and sbss-12
    @Override
    @Transactional
    public void confirmMembers(int heistId, List<String> heistMemberNames) {

//        check if heist exists and has status planning
        final Heist heist = findById(heistId);
        if (heist.getStatus() != HeistStatus.PLANNING)
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED);

        final List<Member> eligibleMembers = findEligibleMembers(heistId);
        final List<String> eligibleMemberNames = eligibleMembers.stream()
                .map(eligibleMemberName -> eligibleMemberName.getName())
                .collect(Collectors.toList());
        final List<Member> confirmedMembers = new ArrayList<>();
//        check if all provided names are in eligible members list
        for (String memberName : heistMemberNames) {
            if (!eligibleMemberNames.contains(memberName))
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            confirmedMembers.add(memberService.findByName(memberName));
        }
        heist.setMembers(confirmedMembers);
        heist.setStatus(HeistStatus.READY);
        for (Member confirmedMember : confirmedMembers) {
            emailService.sendEmail(confirmedMember.getEmail(), "participation",
                    "You have been confirmed to participate in a heist!\n\nDetails:\n\n" + heist.toString());
        }
        heistRepository.save(heist);
    }


    //  sbss-08
    @Override
    @Transactional
    public void start(int heistId) {
        Heist heist = findById(heistId);
        if (heist.getStatus() != HeistStatus.READY)
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED);
        heist.setStatus(HeistStatus.IN_PROGRESS);
        heistRepository.save(heist);
//        send mail to members
        for (Member member : heist.getMembers()) {
            emailService.sendEmail(member.getEmail(), "start",
                    "Heist '" + heist.getName() + "' has started!");
        }
    }

    //    sbss-10
    @Override
    @Transactional
    public void end(int heistId) {
        final Heist heist = findById(heistId);
        if (heist.getStatus() != HeistStatus.IN_PROGRESS)
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED);
        heist.setStatus(HeistStatus.FINISHED);

        // sbss-11 - set outcome
//      count required members for this heist
        int requiredMembers = 0;
        for (HeistSkill heistSkill : heist.getHeistSkills()) {
            requiredMembers += heistSkill.getMembers();
        }

//        members that we confirmed for this heist
        final List<Member> confirmedMembers = heist.getMembers();
        final int numberOfConfirmedMembers = confirmedMembers.size();

        final double ratio = (double) numberOfConfirmedMembers / requiredMembers;

//      FAILED, all members EXPIRED or INCARCERATED
        if (ratio < 0.5) {
            for (Member confirmedMember : confirmedMembers) {
                setRandomStatus(confirmedMember);
            }
            heist.setOutcome(Outcome.FAILED);


        } else if (ratio >= 0.5 && ratio < 0.75) {
            Collections.shuffle(confirmedMembers);

//      FAILED, 2/3 of the members EXPIRED or INCARCERATED
            if (Math.random() < 0.5) {
                for (int i = 0; i < numberOfConfirmedMembers * 2 / 3; i++)
                    setRandomStatus(confirmedMembers.get(i));
                heist.setOutcome(Outcome.FAILED);

//      SUCCEEDED, 1/3 of the members EXPIRED or INCARCERATED
            } else {
                for (int i = 0; i < numberOfConfirmedMembers / 3; i++)
                    setRandomStatus(confirmedMembers.get(i));
                heist.setOutcome(Outcome.SUCCEEDED);
            }

//      SUCCEEDED, 1/3 of the members INCARCERATED
        } else if (ratio >= 0.75 && ratio < 1) {
            Collections.shuffle(confirmedMembers);
            for (int i = 0; i < numberOfConfirmedMembers / 3; i++)
                setRandomStatus(confirmedMembers.get(i));
            heist.setOutcome(Outcome.SUCCEEDED);

//      SUCCEEDED
        } else {
            heist.setOutcome(Outcome.SUCCEEDED);
        }
        heistRepository.save(heist);

//       send mail to members
        for (Member member : heist.getMembers()) {
            emailService.sendEmail(member.getEmail(), "finish",
                    "Heist '" + heist.getName() + "' has finished!");
        }

        updateSkills(heist);
    }


    //    sbss-10
    @Override
    public List<Heist> findByStartTimeBefore(Timestamp before) {
        return heistRepository.findByStartTimeBefore(before);
    }

    @Override
    public List<Heist> findByEndTimeBefore(Timestamp before) {
        return heistRepository.findByEndTimeBefore(before);
    }


    //    sbss-11
    public Outcome viewOutcome(int heistId) {
        final Heist heist = findById(heistId);
        if (heist.getStatus() != HeistStatus.FINISHED)
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED);
        return heist.getOutcome();
    }


    private void setRandomStatus(Member member) {
        final double random = Math.random();
        if (random < 0.5)
            member.setStatus(Status.EXPIRED);
        else
            member.setStatus(Status.INCARCERATED);
    }


    // sbss-13
    private void updateSkills(Heist heist) {
        long duration = (heist.getEndTime().getTime() - heist.getStartTime().getTime()) / 1000;
        long numberOfAsterisksToAdd = duration / memberSkillProperties.getLevelUpTime();

        for (Member member : heist.getMembers()) {
            Set<MemberSkill> skillsToUpdate = new HashSet<>();

            for (MemberSkill memberSkill : member.getMemberSkills()) {
                for (HeistSkill heistSkill : heist.getHeistSkills()) {
                    if (memberSkill.getMemberSkillId().getSkillId().equals(heistSkill.getHeistSkillId().getSkillId())
                            && memberSkill.getLevel().length() >= heistSkill.getHeistSkillId().getLevel().length())
                        skillsToUpdate.add(memberSkill);
                }
            }
            for (MemberSkill skillToUpdate : skillsToUpdate) {
                String newLevel = setAsterisks(skillToUpdate.getLevel(), (int) numberOfAsterisksToAdd);
                skillToUpdate.setLevel(newLevel);
            }
        }
    }

    private String setAsterisks(String currentLevel, int numberOfAsterisksToAdd) {
        int numberOfAsterisks = Math.min(currentLevel.length() + numberOfAsterisksToAdd, 10);
        String asterisksString = "*";
        return asterisksString.repeat(numberOfAsterisks);
    }
}
