package org.agency04.moneyheist.service;

import org.agency04.moneyheist.entities.Skill;

public interface SkillService {

    Skill findByName(String name);

    Skill addSkill(Skill skill);

}
