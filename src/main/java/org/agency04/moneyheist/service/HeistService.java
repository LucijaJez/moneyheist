package org.agency04.moneyheist.service;

import org.agency04.moneyheist.dto.HeistSkillDto;
import org.agency04.moneyheist.entities.Heist;
import org.agency04.moneyheist.entities.HeistSkill;
import org.agency04.moneyheist.entities.Member;
import org.agency04.moneyheist.entities.Outcome;

import java.sql.Timestamp;
import java.util.List;

public interface HeistService {

    List<Heist> findAll();

    Heist findById(int heistId);

    List<Member> getHeistMembers(int heistId);

    List<HeistSkill> getHeistSkills(int heistId);

    Heist addHeist(Heist heist);

    void updateSkills(int heistId, List<HeistSkillDto> skillsDto);

    List<Member> findEligibleMembers(int heistId);

    void confirmMembers(int heistId, List<String> heistMemberNames);

    void start(int heistId);

    void end(int heistId);

    List<Heist> findByStartTimeBefore(Timestamp before);

    List<Heist> findByEndTimeBefore(Timestamp before);

    Outcome viewOutcome(int heistId);

}
