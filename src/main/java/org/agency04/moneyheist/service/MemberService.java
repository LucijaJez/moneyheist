package org.agency04.moneyheist.service;

import org.agency04.moneyheist.dto.SkillsDto;
import org.agency04.moneyheist.entities.Member;

import java.util.List;

public interface MemberService {

    List<Member> findAll();

    Member findById(int memberId);

    Member findByName(String name);

    Member addMember(Member member);

    void updateSkills(int memberId, SkillsDto skillsDto);

    void deleteSkill(int memberId, String skillName);

    Member saveMember(Member member);
}
