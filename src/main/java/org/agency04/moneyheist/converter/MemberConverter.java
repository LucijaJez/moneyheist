package org.agency04.moneyheist.converter;

import org.agency04.moneyheist.dto.MemberDto;
import org.agency04.moneyheist.dto.MemberSkillDto;
import org.agency04.moneyheist.entities.Member;
import org.agency04.moneyheist.entities.MemberSkill;
import org.agency04.moneyheist.entities.MemberSkillId;
import org.agency04.moneyheist.entities.Skill;
import org.agency04.moneyheist.service.SkillService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MemberConverter {

    private final SkillService skillService;
    private final ModelMapper modelMapper;

    @Autowired
    public MemberConverter(SkillService skillService, ModelMapper modelMapper) {
        this.skillService = skillService;
        this.modelMapper = modelMapper;
    }


    public MemberDto entityToDto(Member member) {
        final MemberDto memberDto = modelMapper.map(member, MemberDto.class);
        if (member.getMainSkill() != null)
            memberDto.setMainSkill(member.getMainSkill().getName());
        final List<MemberSkillDto> memberSkillDtos = member.getMemberSkills().stream()
                .map(memberSkill -> new MemberSkillDto(memberSkill.getSkill().getName(), memberSkill.getLevel()))
                .collect(Collectors.toList());
        memberDto.setSkills(memberSkillDtos);
        return memberDto;
    }

    public Member dtoToEntity(MemberDto memberDto) {
        final Member member = modelMapper.map(memberDto, Member.class);
        Skill mainSkill = skillService.findByName(memberDto.getMainSkill());
        if (mainSkill == null) {
            mainSkill = new Skill(memberDto.getMainSkill());
        }
        if (memberDto.getMainSkill() != null)
            member.setMainSkill(mainSkill);
        final List<MemberSkill> memberSkills = new ArrayList<>();
        for (MemberSkillDto memberSkillDto : memberDto.getSkills()) {
            Skill skill = skillService.findByName(memberSkillDto.getName());
            if (skill == null)
                skill = new Skill(memberSkillDto.getName());
            if (skill.equals(mainSkill))
                skill = mainSkill;
            MemberSkill memberSkill = new MemberSkill();
            memberSkill.setMemberSkillId(new MemberSkillId());
            memberSkill.setLevel(memberSkillDto.getLevel());
            memberSkill.setMember(member);
            memberSkill.setSkill(skill);
            memberSkills.add(memberSkill);
        }
        member.setMemberSkills(memberSkills);
        return member;
    }

    public List<MemberDto> entityToDto(List<Member> members) {
        return members.stream().map(member -> entityToDto(member)).collect(Collectors.toList());
    }

    public List<Member> dtoToEntity(List<MemberDto> memberDtos) {
        return memberDtos.stream().map(memberDto -> dtoToEntity(memberDto)).collect(Collectors.toList());
    }
}
