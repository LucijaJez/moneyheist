package org.agency04.moneyheist.converter;

import org.agency04.moneyheist.dto.MemberSkillDto;
import org.agency04.moneyheist.dto.MemberWithIdDto;
import org.agency04.moneyheist.entities.Member;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.stream.Collectors;

@Component
public class MemberWithIdConverter {

    private final ModelMapper modelMapper;

    @Autowired
    public MemberWithIdConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }


    public MemberWithIdDto entityToDto(Member member) {
        final MemberWithIdDto memberWithIdDto = modelMapper.map(member, MemberWithIdDto.class);
        if (member.getMainSkill() != null)
            memberWithIdDto.setMainSkill(member.getMainSkill().getName());
        final List<MemberSkillDto> memberSkillDtos = member.getMemberSkills().stream()
                .map(memberSkill -> new MemberSkillDto(memberSkill.getSkill().getName(), memberSkill.getLevel()))
                .collect(Collectors.toList());
        memberWithIdDto.setSkills(memberSkillDtos);
        return memberWithIdDto;
    }


    public List<MemberWithIdDto> entityToDto(List<Member> members) {
        return members.stream().map(member -> entityToDto(member)).collect(Collectors.toList());
    }

}
