package org.agency04.moneyheist.converter;

import org.agency04.moneyheist.dto.HeistDto;
import org.agency04.moneyheist.dto.HeistSkillDto;
import org.agency04.moneyheist.dto.HeistWithIdDto;
import org.agency04.moneyheist.entities.Heist;
import org.agency04.moneyheist.entities.HeistSkill;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class HeistWithIdConverter {

    private final ModelMapper modelMapper;

    @Autowired
    public HeistWithIdConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }


    public HeistWithIdDto entityToDto(Heist heist) {
        final HeistWithIdDto heistWithIdDto = modelMapper.map(heist, HeistWithIdDto.class);
        final List<HeistSkill> heistSkills = heist.getHeistSkills();
        final List<HeistSkillDto> heistSkillDtos = new ArrayList<>();
        for (HeistSkill heistSkill : heistSkills) {
            HeistSkillDto heistSkillDto = new HeistSkillDto();
            heistSkillDto.setName(heistSkill.getSkill().getName());
            heistSkillDto.setLevel(heistSkill.getHeistSkillId().getLevel());
            heistSkillDto.setMembers(heistSkill.getMembers());
            heistSkillDtos.add(heistSkillDto);
        }
        heistWithIdDto.setSkills(heistSkillDtos);
        return heistWithIdDto;
    }

    public List<HeistWithIdDto> entityToDto(List<Heist> heists) {
        return heists.stream().map(heist -> entityToDto(heist)).collect(Collectors.toList());
    }

}
