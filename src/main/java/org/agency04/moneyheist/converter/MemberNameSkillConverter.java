package org.agency04.moneyheist.converter;

import org.agency04.moneyheist.dto.MemberNameSkillDto;
import org.agency04.moneyheist.entities.Member;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MemberNameSkillConverter {

    private final MemberSkillConverter memberSkillConverter;

    @Autowired
    public MemberNameSkillConverter(MemberSkillConverter memberSkillConverter) {
        this.memberSkillConverter = memberSkillConverter;
    }

    public MemberNameSkillDto entityToDto(Member member) {
        MemberNameSkillDto memberNameSkillDto = new MemberNameSkillDto();
        memberNameSkillDto.setName(member.getName());
        memberNameSkillDto.setSkills(memberSkillConverter.entityToDto(member.getMemberSkills()));
        return memberNameSkillDto;
    }

    public List<MemberNameSkillDto> entityToDto(List<Member> members) {
        return members.stream().map(member -> entityToDto(member)).collect(Collectors.toList());
    }
}
