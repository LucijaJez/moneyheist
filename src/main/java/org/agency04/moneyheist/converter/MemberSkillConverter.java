package org.agency04.moneyheist.converter;

import org.agency04.moneyheist.dto.MemberSkillDto;
import org.agency04.moneyheist.entities.MemberSkill;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MemberSkillConverter {

    public MemberSkillDto entityToDto(MemberSkill memberSkill) {
        final MemberSkillDto memberSkillDto = new MemberSkillDto();
        memberSkillDto.setName(memberSkill.getSkill().getName());
        memberSkillDto.setLevel(memberSkill.getLevel());
        return memberSkillDto;
    }

    public List<MemberSkillDto> entityToDto(List<MemberSkill> memberSkills) {
        return memberSkills.stream().map(memberSkill -> entityToDto(memberSkill)).collect(Collectors.toList());
    }
}
