package org.agency04.moneyheist.converter;

import org.agency04.moneyheist.dto.HeistSkillDto;
import org.agency04.moneyheist.entities.HeistSkill;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class HeistSkillConverter {

    private final ModelMapper modelMapper;

    @Autowired
    public HeistSkillConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public HeistSkillDto entityToDto(HeistSkill heistSkill) {
        final HeistSkillDto heistSkillDto = modelMapper.map(heistSkill, HeistSkillDto.class);
        heistSkillDto.setLevel(heistSkill.getHeistSkillId().getLevel());
        heistSkillDto.setName(heistSkill.getSkill().getName());
        return heistSkillDto;
    }


    public List<HeistSkillDto> entityToDto(List<HeistSkill> heistSkills) {
        return heistSkills.stream().map(heistSkill -> entityToDto(heistSkill)).collect(Collectors.toList());
    }
}
