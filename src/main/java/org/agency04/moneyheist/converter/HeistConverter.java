package org.agency04.moneyheist.converter;

import org.agency04.moneyheist.dto.HeistDto;
import org.agency04.moneyheist.dto.HeistSkillDto;
import org.agency04.moneyheist.entities.*;
import org.agency04.moneyheist.service.SkillService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class HeistConverter {

    private final SkillService skillService;
    private final ModelMapper modelMapper;

    @Autowired
    public HeistConverter(SkillService skillService, ModelMapper modelMapper) {
        this.skillService = skillService;
        this.modelMapper = modelMapper;
    }


    public HeistDto entityToDto(Heist heist) {
        final HeistDto heistDto = modelMapper.map(heist, HeistDto.class);
        final List<HeistSkill> heistSkills = heist.getHeistSkills();
        final List<HeistSkillDto> heistSkillDtos = new ArrayList<>();
        for (HeistSkill heistSkill : heistSkills) {
            HeistSkillDto heistSkillDto = new HeistSkillDto();
            heistSkillDto.setName(heistSkill.getSkill().getName());
            heistSkillDto.setLevel(heistSkill.getHeistSkillId().getLevel());
            heistSkillDto.setMembers(heistSkill.getMembers());
            heistSkillDtos.add(heistSkillDto);
        }
        heistDto.setSkills(heistSkillDtos);
        return heistDto;
    }

    public Heist dtoToEntity(HeistDto heistDto) {
        final Heist heist = modelMapper.map(heistDto, Heist.class);
        final List<HeistSkill> heistSkills = new ArrayList<>();
        final Map<String, Skill> newSkills = new HashMap<>();

        for (HeistSkillDto heistSkillDto : heistDto.getSkills()) {
            final String newSkillName = heistSkillDto.getName();
            Skill skill = skillService.findByName(newSkillName);
//          skill not in db
            if (skill == null) {
//          if multiple new skills with the same name were provided, save new skill only once
                if (newSkills.containsKey(newSkillName)) {
                    skill = newSkills.get(newSkillName);
                } else {
                    skill = new Skill(newSkillName);
                    newSkills.put(newSkillName, skill);
                }
            }

            HeistSkill heistSkill = new HeistSkill();
            heistSkill.setHeistSkillId(new HeistSkillId());
            heistSkill.getHeistSkillId().setLevel(heistSkillDto.getLevel());
            heistSkill.setMembers(heistSkillDto.getMembers());
            heistSkill.setHeist(heist);
            heistSkill.setSkill(skill);
            heistSkills.add(heistSkill);
        }

        heist.setHeistSkills(heistSkills);
        return heist;
    }

    public List<HeistDto> entityToDto(List<Heist> heists) {
        return heists.stream().map(heist -> entityToDto(heist)).collect(Collectors.toList());
    }

    public List<Heist> dtoToEntity(List<HeistDto> heistDtos) {
        return heistDtos.stream().map(heistDto -> dtoToEntity(heistDto)).collect(Collectors.toList());
    }
}
