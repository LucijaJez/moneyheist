package org.agency04.moneyheist.controller;

import org.agency04.moneyheist.converter.*;
import org.agency04.moneyheist.dto.*;
import org.agency04.moneyheist.entities.Heist;
import org.agency04.moneyheist.entities.HeistSkill;
import org.agency04.moneyheist.entities.Member;
import org.agency04.moneyheist.entities.Outcome;
import org.agency04.moneyheist.service.HeistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value = "/heist")
public class HeistController {

    private final HeistService heistService;
    private final HeistConverter heistConverter;
    private final HeistSkillConverter heistSkillConverter;
    private final MemberNameSkillConverter memberNameSkillConverter;
    private final MemberConverter memberConverter;
    private final HeistWithIdConverter heistWithIdConverter;


    @Autowired
    public HeistController(HeistService heistService, HeistConverter heistConverter, HeistSkillConverter heistSkillConverter, MemberNameSkillConverter memberNameSkillConverter, MemberConverter memberConverter, HeistWithIdConverter heistWithIdConverter) {
        this.heistService = heistService;
        this.heistConverter = heistConverter;
        this.heistSkillConverter = heistSkillConverter;
        this.memberNameSkillConverter = memberNameSkillConverter;
        this.memberConverter = memberConverter;
        this.heistWithIdConverter = heistWithIdConverter;
    }

    @GetMapping
    public ResponseEntity<List<HeistWithIdDto>> findAll() {
        final List<Heist> heists = heistService.findAll();
        return ResponseEntity.of(Optional.of(heistWithIdConverter.entityToDto(heists)));
    }


    //  sbss-09 - 3.
    @GetMapping("/{heist_id}")
    public ResponseEntity<HeistDto> findById(@PathVariable(name = "heist_id") int heistId) {
        final Heist heist = heistService.findById(heistId);
        final HeistDto heistDto = heistConverter.entityToDto(heist);
        return new ResponseEntity<>(heistDto, HttpStatus.OK);
    }


    //  sbss-09 - 4.
    @GetMapping("/{heist_id}/members")
    public ResponseEntity<List<MemberNameSkillDto>> getHeistMembers(@PathVariable(name = "heist_id") int heistId) {
        final List<Member> members = heistService.getHeistMembers(heistId);
        List<MemberNameSkillDto> memberNameSkillDtos = memberNameSkillConverter.entityToDto(members);
        return new ResponseEntity<>(memberNameSkillDtos, HttpStatus.OK);
    }


    //  sbss-09 - 5.
    @GetMapping("/{heist_id}/skills")
    public ResponseEntity<List<HeistSkillDto>> getHeistSkills(@PathVariable(name = "heist_id") int heistId) {
        List<HeistSkill> heistSkills = heistService.getHeistSkills(heistId);
        List<HeistSkillDto> heistSkillDtos = heistSkillConverter.entityToDto(heistSkills);
        return new ResponseEntity<>(heistSkillDtos, HttpStatus.OK);
    }


    //  sbss-09 - 6.
    @GetMapping("/{heist_id}/status")
    public ResponseEntity<HeistStatusDto> getHeistStatus(@PathVariable(name = "heist_id") int heistId) {
        Heist heist = heistService.findById(heistId);
        HeistStatusDto heistStatusDto = new HeistStatusDto(heist.getStatus());
        return new ResponseEntity<>(heistStatusDto, HttpStatus.OK);
    }


    //  sbss-04
    @PostMapping
    public ResponseEntity<Void> addHeist(@RequestBody HeistDto heistDto) {

//      multiple skills with the same name and level provided
        final long size = heistDto.getSkills().size();
        final long sizeDistinct = heistDto.getSkills().stream()
                .map(heistSkillDto -> Pair.of(heistSkillDto.getName(), heistSkillDto.getLevel()))
                .distinct().count();
        if (size != sizeDistinct)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        final Heist heist = heistConverter.dtoToEntity(heistDto);
        final Heist addedHeist = heistService.addHeist(heist);
        return ResponseEntity
                .created(URI.create("/heist/" + addedHeist.getId()))
                .header("Access-Control-Expose-Headers","Location")
                .build();
    }

    //  sbss-05
    @PatchMapping("/{heist_id}/skills")
    public ResponseEntity<Void> updateSkills(@PathVariable(name = "heist_id") int heistId,
                                             @RequestBody UpdatedSkillsDto updatedSkillsDto) {
        List<HeistSkillDto> heistSkillsDto = updatedSkillsDto.getSkills();
        heistService.updateSkills(heistId, heistSkillsDto);
        final HttpHeaders responseHeaders = new HttpHeaders();
        final String uri = "/heist/" + heistId + "/skills";
        responseHeaders.set("Content-Location", uri);
        return ResponseEntity.noContent().headers(responseHeaders).build();
    }

    //  sbss-06
    @GetMapping("/{heist_id}/eligible_members")
    public ResponseEntity<EligibleMembersDto> findEligibleMembers(@PathVariable(name = "heist_id") int heistId) {

        final List<Member> eligibleMembers = heistService.findEligibleMembers(heistId);
        List<MemberDto> memberDtos = memberConverter.entityToDto(eligibleMembers);

        Heist heist = heistService.findById(heistId);
        final List<HeistSkill> heistSkills = heist.getHeistSkills();
        final List<HeistSkillDto> heistSkillDtos = heistSkillConverter.entityToDto(heistSkills);

        final EligibleMembersDto eligibleMembersDto = new EligibleMembersDto(heistSkillDtos, memberDtos);

        return ResponseEntity.of(Optional.of(eligibleMembersDto));
    }


    //  sbss-07
    @PutMapping("/{heist_id}/members")
    public ResponseEntity<Void> confirmMembers(@PathVariable(name = "heist_id") int heistId,
                                               @RequestBody HeistMemberNamesDto heistMemberNamesDto) {
        heistService.confirmMembers(heistId, heistMemberNamesDto.getMembers());
        final HttpHeaders responseHeaders = new HttpHeaders();
        final String uri = "/heist/" + heistId + "/members";
        responseHeaders.set("Content-Location", uri);
        return ResponseEntity.noContent().headers(responseHeaders).build();
    }


    //  sbss-08
    @PutMapping("/{heist_id}/start")
    public ResponseEntity<Void> start(@PathVariable(name = "heist_id") int heistId) {
        heistService.start(heistId);
        final HttpHeaders responseHeaders = new HttpHeaders();
        final String uri = "/heist/" + heistId + "/status";
        responseHeaders.set("Location", uri);
        return ResponseEntity.ok().headers(responseHeaders).build();
    }


    //  sbss-11
    @GetMapping("/{heist_id}/outcome")
    public ResponseEntity<OutcomeDto> viewOutcome(@PathVariable(name = "heist_id") int heistId) {
        Outcome outcome = heistService.viewOutcome(heistId);
        return new ResponseEntity<>(new OutcomeDto(outcome), HttpStatus.OK);
    }

}
