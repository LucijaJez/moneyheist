package org.agency04.moneyheist.controller;

import org.agency04.moneyheist.converter.MemberConverter;
import org.agency04.moneyheist.converter.MemberWithIdConverter;
import org.agency04.moneyheist.dto.MemberDto;
import org.agency04.moneyheist.dto.MemberSkillDto;
import org.agency04.moneyheist.dto.MemberWithIdDto;
import org.agency04.moneyheist.dto.SkillsDto;
import org.agency04.moneyheist.entities.Member;
import org.agency04.moneyheist.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value = "/member")
public class MemberController {

    private final MemberService memberService;
    private final MemberConverter memberConverter;
    private final MemberWithIdConverter memberWithIdConverter;


    @Autowired
    public MemberController(MemberService memberService, MemberConverter memberConverter, MemberWithIdConverter memberWithIdConverter) {
        this.memberService = memberService;
        this.memberConverter = memberConverter;
        this.memberWithIdConverter = memberWithIdConverter;
    }


    @GetMapping
    public ResponseEntity<List<MemberWithIdDto>> findAll() {
        final List<Member> members = memberService.findAll();
        return ResponseEntity.of(Optional.of(memberWithIdConverter.entityToDto(members)));
    }


    //  sbss-09 - 1.
    @GetMapping("/{member_id}")
    public ResponseEntity<MemberDto> findById(@PathVariable(name = "member_id") int memberId) {
        final Member member = memberService.findById(memberId);
        final MemberDto memberDto = memberConverter.entityToDto(member);
        return new ResponseEntity<>(memberDto, HttpStatus.OK);
    }


    //  sbss-09 - 2.
    @GetMapping("/{member_id}/skills")
    public ResponseEntity<SkillsDto> getMemberSkills(@PathVariable(name = "member_id") int memberId) {
        final Member member = memberService.findById(memberId);
        final List<MemberSkillDto> memberSkillDtos = member.getMemberSkills().stream()
                .map(memberSkill -> new MemberSkillDto(memberSkill.getSkill().getName(), memberSkill.getLevel()))
                .collect(Collectors.toList());
        SkillsDto skillsDto = new SkillsDto(memberSkillDtos, (member.getMainSkill() == null ? null : member.getMainSkill().getName()));
        return new ResponseEntity<>(skillsDto, HttpStatus.OK);
    }


    //  sbss-01
    @PostMapping
    public ResponseEntity<Void> addMember(@RequestBody MemberDto memberDto) {
        final long size = memberDto.getSkills().size();
        final long sizeDistinct = memberDto.getSkills().stream()
                .map(memberSkillDto -> memberSkillDto.getName())
                .distinct().count();
        if (size != sizeDistinct)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        final Member member = memberConverter.dtoToEntity(memberDto);
        final Member addedMember = memberService.addMember(member);
        return ResponseEntity
                .created(URI.create("/member/" + addedMember.getId()))
                .header("Access-Control-Expose-Headers","Location")
                .build();
    }


    //  sbss-02
    @PutMapping("/{member_id}/skills")
    public ResponseEntity<Void> updateSkills(@PathVariable(name = "member_id") int memberId,
                                             @RequestBody SkillsDto skillsDto) {
        memberService.updateSkills(memberId, skillsDto);
        final HttpHeaders responseHeaders = new HttpHeaders();
        final String uri = "/member/" + memberId + "/skills";
        responseHeaders.set("Content-Location", uri);
        return ResponseEntity.noContent().headers(responseHeaders).build();
    }


    //  sbss-03
    @DeleteMapping("/{member_id}/skills/{skill_name}")
    public ResponseEntity<Void> deleteSkill(@PathVariable(name = "member_id") int memberId,
                                            @PathVariable(name = "skill_name") String skillName) {
        memberService.deleteSkill(memberId, skillName);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
