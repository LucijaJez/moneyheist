package org.agency04.moneyheist.entities;

public enum Status {
    AVAILABLE, EXPIRED, INCARCERATED, RETIRED
}
