package org.agency04.moneyheist.entities;

public enum Outcome {
    FAILED, SUCCEEDED
}
