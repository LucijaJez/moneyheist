package org.agency04.moneyheist.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "MemberSkill")
@Table(name = "member_skill")
public class MemberSkill {

    @EmbeddedId
    private MemberSkillId memberSkillId;

    @Column(name = "level")
    private String level;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("skillId")
    private Skill skill;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("memberId")
    private Member member;

    public MemberSkill() {
    }


    public MemberSkill(MemberSkillId memberSkillId, String level) {
        this.memberSkillId = memberSkillId;
        this.level = level;
    }

    public MemberSkillId getMemberSkillId() {
        return memberSkillId;
    }

    public void setMemberSkillId(MemberSkillId memberSkillId) {
        this.memberSkillId = memberSkillId;
    }


    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MemberSkill that = (MemberSkill) o;
        return Objects.equals(memberSkillId, that.memberSkillId) &&
                Objects.equals(level, that.level) &&
                Objects.equals(skill, that.skill) &&
                Objects.equals(member, that.member);
    }

    @Override
    public int hashCode() {
        return Objects.hash(memberSkillId, level, skill, member);
    }
}

