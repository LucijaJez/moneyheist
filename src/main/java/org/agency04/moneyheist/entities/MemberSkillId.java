package org.agency04.moneyheist.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class MemberSkillId implements Serializable {

    @Column(name = "member_id")
    private Integer memberId;

    @Column(name = "skill_id")
    private Integer skillId;

    public MemberSkillId() {
    }

    public MemberSkillId(Integer memberId, Integer skillId) {
        this.memberId = memberId;
        this.skillId = skillId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getSkillId() {
        return skillId;
    }

    public void setSkillId(Integer skillId) {
        this.skillId = skillId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MemberSkillId that = (MemberSkillId) o;
        return Objects.equals(memberId, that.memberId) &&
                Objects.equals(skillId, that.skillId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(memberId, skillId);
    }
}
