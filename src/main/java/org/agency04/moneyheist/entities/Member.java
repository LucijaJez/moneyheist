package org.agency04.moneyheist.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity(name = "Member")
@Table(name = "member")
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "sex", nullable = false)
    private Sex sex;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "main_skill", referencedColumnName = "id")
    private Skill mainSkill;

    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<MemberSkill> memberSkills = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "heist_member",
            joinColumns = {@JoinColumn(name = "member_id")},
            inverseJoinColumns = {@JoinColumn(name = "heist_id")})
    private List<Heist> heists;


    public Member() {
    }

    public Member(String name, Sex sex, String email, Skill skill, Status status) {
        this.name = name;
        this.sex = sex;
        this.email = email;
        this.mainSkill = skill;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Skill getMainSkill() {
        return mainSkill;
    }

    public void setMainSkill(Skill skill) {
        this.mainSkill = skill;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<MemberSkill> getMemberSkills() {
        return memberSkills;
    }

    public void setMemberSkills(List<MemberSkill> memberSkills) {
        this.memberSkills = memberSkills;
    }

    public List<Heist> getHeists() {
        return heists;
    }

    public void setHeists(List<Heist> heists) {
        this.heists = heists;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        return Objects.equals(id, member.id) &&
                Objects.equals(name, member.name) &&
                sex == member.sex &&
                Objects.equals(email, member.email) &&
                Objects.equals(memberSkills, member.memberSkills) &&
                Objects.equals(mainSkill, member.mainSkill) &&
                status == member.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, sex, email, memberSkills, mainSkill, status);
    }
}

