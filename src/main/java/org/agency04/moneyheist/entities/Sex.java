package org.agency04.moneyheist.entities;

public enum Sex {
    F, M
}
