package org.agency04.moneyheist.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "HeistSkill")
@Table(name = "heist_skill")
public class HeistSkill {

    @EmbeddedId
    private HeistSkillId heistSkillId;

    @Column(name = "members")
    private Integer members;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("skillId")
    private Skill skill;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("heistId")
    private Heist heist;

    public HeistSkill() {
    }

    public HeistSkill(HeistSkillId heistSkillId, Integer members, Skill skill, Heist heist) {
        this.heistSkillId = heistSkillId;
        this.members = members;
        this.skill = skill;
        this.heist = heist;
    }

    public HeistSkillId getHeistSkillId() {
        return heistSkillId;
    }

    public void setHeistSkillId(HeistSkillId heistSkillId) {
        this.heistSkillId = heistSkillId;
    }

    public Integer getMembers() {
        return members;
    }

    public void setMembers(Integer members) {
        this.members = members;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public Heist getHeist() {
        return heist;
    }

    public void setHeist(Heist heist) {
        this.heist = heist;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HeistSkill that = (HeistSkill) o;
        return Objects.equals(heistSkillId, that.heistSkillId) &&
                Objects.equals(members, that.members) &&
                Objects.equals(skill, that.skill) &&
                Objects.equals(heist, that.heist);
    }

    @Override
    public int hashCode() {
        return Objects.hash(heistSkillId, members, skill, heist);
    }
}

