package org.agency04.moneyheist.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class HeistSkillId implements Serializable {

    @Column(name = "heist_id")
    private Integer heistId;

    @Column(name = "skill_id")
    private Integer skillId;

    @Column(name = "level")
    private String level;

    public HeistSkillId() {
    }

    public HeistSkillId(Integer heistId, Integer skillId, String level) {
        this.heistId = heistId;
        this.skillId = skillId;
        this.level = level;
    }

    public Integer getHeistId() {
        return heistId;
    }

    public void setHeistId(Integer heistId) {
        this.heistId = heistId;
    }

    public Integer getSkillId() {
        return skillId;
    }

    public void setSkillId(Integer skillId) {
        this.skillId = skillId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HeistSkillId that = (HeistSkillId) o;
        return Objects.equals(heistId, that.heistId) &&
                Objects.equals(skillId, that.skillId) &&
                Objects.equals(level, that.level);
    }

    @Override
    public int hashCode() {
        return Objects.hash(heistId, skillId, level);
    }
}
