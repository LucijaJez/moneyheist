package org.agency04.moneyheist.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Heist")
@Table(name = "heist")
public class Heist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "location")
    private String location;

    @Column(name = "start_time")
    private Timestamp startTime;

    @Column(name = "end_time")
    private Timestamp endTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private HeistStatus status = HeistStatus.PLANNING;

    @Enumerated(EnumType.STRING)
    @Column(name = "outcome")
    private Outcome outcome;

    @OneToMany(mappedBy = "heist", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<HeistSkill> heistSkills = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "heist_member",
            joinColumns = {@JoinColumn(name = "heist_id")},
            inverseJoinColumns = {@JoinColumn(name = "member_id")})
    private List<Member> members;

    public Heist() {
    }

    public Heist(Integer id, String name, String location, Timestamp startTime, Timestamp endTime, List<HeistSkill> heistSkills) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.startTime = startTime;
        this.endTime = endTime;
        this.heistSkills = heistSkills;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public List<HeistSkill> getHeistSkills() {
        return heistSkills;
    }

    public void setHeistSkills(List<HeistSkill> heistSkills) {
        this.heistSkills = heistSkills;
    }

    public HeistStatus getStatus() {
        return status;
    }

    public void setStatus(HeistStatus status) {
        this.status = status;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public void setOutcome(Outcome outcome) {
        this.outcome = outcome;
    }

    @Override
    public String toString() {
        return "Heist name: " + name +
                "\nLocation: " + location +
                "\nStart time: " + startTime;
    }
}

