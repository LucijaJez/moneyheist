package org.agency04.moneyheist.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity(name = "Skill")
@Table(name = "skill")
public class Skill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "skill", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<MemberSkill> skillMembers = new ArrayList<>();

    public Skill() {
    }

    public Skill(String name) {
        if (name != null)
            this.name = name.toLowerCase();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null)
            this.name = name.toLowerCase();
    }

    public List<MemberSkill> getSkillMembers() {
        return skillMembers;
    }

    public void setSkillMembers(List<MemberSkill> skillMembers) {
        this.skillMembers = skillMembers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Skill skill = (Skill) o;
        return Objects.equals(id, skill.id) &&
                Objects.equals(name, skill.name) &&
                Objects.equals(skillMembers, skill.skillMembers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, skillMembers);
    }
}
