package org.agency04.moneyheist.entities;

public enum HeistStatus {
    PLANNING, READY, IN_PROGRESS, FINISHED
}
