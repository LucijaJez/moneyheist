package org.agency04.moneyheist.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties(prefix = "member-skill")
public class MemberSkillProperties {

    private Long levelUpTime;

    public Long getLevelUpTime() {
        return levelUpTime;
    }

    public void setLevelUpTime(Long levelUpTime) {
        this.levelUpTime = levelUpTime;
    }
}
