package org.agency04.moneyheist.repository;

import org.agency04.moneyheist.entities.Heist;
import org.agency04.moneyheist.entities.Member;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface HeistRepository extends JpaRepository<Heist, Integer> {

    Boolean existsByName(String name);

    @Query("SELECT m FROM Member m WHERE m.status IN ('AVAILABLE', 'RETIRED') AND m.id IN " +
            "(SELECT DISTINCT ms.memberSkillId.memberId FROM MemberSkill ms " +
            "JOIN HeistSkill hs ON hs.heistSkillId.skillId = ms.memberSkillId.skillId " +
            "WHERE hs.heistSkillId.heistId = ?1 AND LENGTH(ms.level)>=LENGTH(hs.heistSkillId.level))")
    List<Member> findEligibleMembers(int heistId);

    @Query("SELECT h FROM Heist h WHERE h.startTime < ?1 AND h.status = 'READY'")
    List<Heist> findByStartTimeBefore(Timestamp before);

    @Query("SELECT h FROM Heist h WHERE h.endTime < ?1 AND h.status IN ('IN_PROGRESS','READY')")
    List<Heist> findByEndTimeBefore(Timestamp before);
}
