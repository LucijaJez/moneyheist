package org.agency04.moneyheist.repository;

import org.agency04.moneyheist.entities.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MemberRepository extends JpaRepository<Member, Integer> {

    Boolean existsByEmail(String email);

    Optional<Member> findByName(String name);
}
